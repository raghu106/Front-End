import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';


export const router: Routes = [{
  path: '', component: LoginComponent
}, {
  path: 'login', component: LoginComponent,
  children: [{
    path: 'home', component: HomeComponent
  }]
}];

export const routes: ModuleWithProviders = RouterModule.forRoot(router);

