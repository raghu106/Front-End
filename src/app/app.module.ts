import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { OnsenModule } from 'ngx-onsenui';

import { AppComponent } from './app.component';
import { Page1Component } from './page1/page1.component';
import { Page2Component } from './page2/page2.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { AnalyserComponent } from './analyser/analyser.component';
import { PaymentsComponent } from './payments/payments.component';
import { AccountsComponent } from './accounts/accounts.component';

import { routes } from './app.routes';
import { AppRouteConfig } from './app.router-config';

// Page components
const pages = [ Page1Component, Page2Component ];

@NgModule({
  declarations: [
    AppComponent,
    ...pages,
    LoginComponent,
    HomeComponent,
    AnalyserComponent,
    PaymentsComponent,
    AccountsComponent
  ],
  entryComponents: [
    ...pages
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    HttpClientModule,
    OnsenModule,
    routes
  ],
  providers: [AppRouteConfig],
  bootstrap: [ AppComponent ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule { }
